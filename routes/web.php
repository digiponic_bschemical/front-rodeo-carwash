<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MenuController@index');
Route::get('/merk', 'MenuController@merk');
Route::get('/type/{merk_id}', 'MenuController@type');
Route::get('/transaksi/{merk_id}/{kendaraan_id}', 'MenuController@transaksi');
Route::post('/save', 'MenuController@save');
Route::post('/print', 'MenuController@printRcp');
Route::get('/done', 'MenuController@done');

Route::get('/v2', 'MenuController@patch');
