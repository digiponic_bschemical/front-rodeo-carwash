<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

class extendController extends Controller
{
    public $baseurl = 'http://osac.digiponic.co.id/public/api';

    // Comparision function
    function date_compare($element1, $element2) {
        $datetime1 = strtotime($element1['created_at']);
        $datetime2 = strtotime($element2['created_at']);
        return $datetime1 - $datetime2;
    }

    public function sync()
    {
        $getData = Curl::to($this->baseurl . '/reservasi/all')->get();
        $online = json_decode($getData, true);

        $data = DB::table('tb_penjualan_jasa')
            ->select('id_cabang', 'kode', 'tanggal', 'tanggal_masuk', 'id_pelanggan', 'nama_pelanggan', 'id_merek_kendaraan', 'merek_kendaraan', 'id_kendaraan', 'nama_kendaraan', 'nomor_polisi', 'subtotal', 'diskon_tipe', 'diskon_nominal', 'total', 'total_slot', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by', 'status_penjualan', 'status_pembayaran', 'metode_pembayaran', 'id_merchant', 'nomor_kartu', 'kode_trace', 'bayar', 'kembalian', 'status', 'is_komposisi')
            ->get();

        $offline = [];
        foreach ($data as $o) {

            $detailBooking = DB::table('tb_penjualan_jasa_detail')
                ->select('id_jasa', 'nama_jasa','harga','durasi')
                ->where('kode_penjualan_jasa', $o->kode)
                ->get();

            $o->detail_jasa = [];
            foreach ($detailBooking as $d) {
                $o->detail_jasa[] = (array)$d;
            }
            // refactoring kode
            $o->kode = 'REG00' . strtotime($o->created_at);
            if ((int)$o->status_penjualan == 28) {
                $o->kode = 'POSJS' . strtotime($o->created_at);
            }

            $offline[] = (array)$o;

        }

        // filtering duplicate data
        $newArray = [];
        foreach($online as $key => $val)
        {
            $similar = 0;
            foreach($offline as $k => $v)
            {
                if($val['created_at'] == $v['created_at'] && $val['tanggal'] == $v['tanggal'])
//                if($val['created_at'] == $v['created_at'])
                {
                    $similar++;
                    break;
                }
            }

            if ($similar == 0){
                $newArray[] = $val;
            }
        }

        /*echo "<pre>";
        print_r($newArray);*/

        // merge array
        $result = array_merge($offline, $newArray);

        // Sort the array
        $created  = array_column($result, 'created_at');
        $checkin  = array_column($result, 'tanggal_masuk');
        array_multisort($created, SORT_ASC, $checkin, SORT_ASC, $result);

        // rewrite records
        DB::table('tb_penjualan_jasa')->truncate();
        DB::table('tb_penjualan_jasa_detail')->truncate();

        $newTbjasa = [];
        $newTbjasadetail = [];

        $no = 1;
        foreach ($result as $r) {
            $details = [];
            $r['id'] = $no;
            foreach ($r['detail_jasa'] as $d){
                $d['kode_penjualan_jasa'] = $r['kode'];
                $d['id_penjualan_jasa'] = $r['id'];
                $details[] = $d;
            }
            unset($r['detail_jasa']);

            $newTbjasa[] = $r;
            $newTbjasadetail[] = $details;

            DB::table('tb_penjualan_jasa')->insert($r);
            DB::table('tb_penjualan_jasa_detail')->insert($details);
            $no++;
        }
        echo "Syncronized !";

        /*function get_fb(){
            var feedback = $.ajax({
                type: "POST",
                url: "feedback.php",
                async: false
            }).success(function(){
                        setTimeout(function(){get_fb();}, 10000);
                    }).responseText;

            $('div.feedback-box').html(feedback);
        }*/
    }
}
