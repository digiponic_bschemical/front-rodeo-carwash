<?php
/**
 * Created by PhpStorm.
 * User: - ASUS -
 * Date: 02/08/2019
 * Time: 17.08
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;

class MenuController extends Controller
{
//    public $baseurl = 'http://osac.digiponic.co.id/public/api';

    public $baseurl = 'http://localhost/rodeo-car-wash/public/api';

    public function index()
    {
        /*return view('welcome',
            [
                'title'   => 'RODEO CAR WASH',
                'scripts' => 'js/printRcp.js'
            ]
        );*/

        // Send a GET request
        $response = Curl::to($this->baseurl . '/kendaraan/merk')->get();
        $result = json_decode($response);
        return view('merk', [
            'title'   => 'Silahkan Pilih Merk Kendaraan Anda',
            'merks'   => array_chunk($result->data, 12),
            'scripts' => ''
        ]);
    }

    /*public function merk()
    {
        // Send a GET request
        $response = Curl::to($this->baseurl . '/kendaraan/merk')->get();
        $result = json_decode($response);
        return view('merk', [
            'title'   => 'Silahkan Pilih Merk Kendaraan Anda',
            'merks'   => array_chunk($result->data, 12),
            'scripts' => ''
        ]);
    }*/

    public function type($merkId)
    {
        $response = Curl::to($this->baseurl . '/kendaraan/daftar/' . $merkId)
            ->get();
        $result = json_decode($response);
        return view('tipe', [
            'title'   => 'SILAHKAN PILIH TIPE KENDARAAN ANDA',
            'types'   => array_chunk($result->data, 12),
            'merk'    => $merkId,
            'scripts' => ''
        ]);
    }

    public function transaksi($merk, $kendaraanId)
    {
        $hours = Curl::to($this->baseurl . '/general/jam-operasi/1')->get();
        $carwash = Curl::to($this->baseurl . '/jasa/list/' . $kendaraanId . '/6')
            ->get();
        $carcare = Curl::to($this->baseurl . '/jasa/list/' . $kendaraanId . '/7')
            ->get();

        return view('transaksi', [
            'title'     => 'SILAHKAN PILIH TIPE KENDARAAN ANDA',
            'merk'      => $merk,
            'kendaraan' => $kendaraanId,
            'jamTutup'  => json_decode($hours)->data->jam_tutup,
            'carwash'   => array_chunk(json_decode($carwash)->data, 3),
            'carcare'   => array_chunk(json_decode($carcare)->data, 8),
            'scripts'   => ''
        ]);
    }

    public function patch()
    {
        $hours = Curl::to($this->baseurl . '/general/jam-operasi/1')->get();
        $carwash = Curl::to($this->baseurl . '/jasa/carwash')
            ->get();

        return view('carwashonly', [
            'title'    => 'SILAHKAN PILIH TIPE KENDARAAN ANDA',
            'jamTutup' => json_decode($hours)->data->jam_tutup,
            'carwash'  => array_chunk(json_decode($carwash)->data, 3),
            'scripts'  => ''
        ]);
    }

    public function printRcp(Request $request)
    {
        $response = Curl::to($this->baseurl . '/reservasi/detail/' . $request->code)
            ->get();
        $result = json_decode($response);
        if ($result == null) {
            return response()->json(['error' => true, 'msg' => 'Maaf, data booking tidak ditemukan', 'data' => null], 200);
        } else {
            return response()->json($result, 200);

//            try {
//                $printerName = Curl::to($this->baseurl . '/general/printerFrontName')->get();
//                $profile = CapabilityProfile::load("simple");
//
//                // if deploy local do not activate this line
//                /*$ip = Request::ip();
//                $connector = new WindowsPrintConnector("smb://Guest@" . $ip . '/' . $printerName);*/
//
//                $connector = new WindowsPrintConnector($printerName);
//                $printer = new Printer($connector);
//                $printer->setJustification(Printer::JUSTIFY_CENTER);
//                $printer->setTextSize(2, 2);
//                $printer->text("RODEO CAR WASH");
//                $printer->text("\n");
//                $printer->setTextSize(2, 2);
//                $printer->text($result->data->status);
//                $printer->text("\n\n");
//
//                $printer->qrCode($result->data->kode, Printer::QR_ECLEVEL_L, 11);
//                $tanggal = $time = explode(" - ", $result->data->tanggal);
//                $tanggal_masuk = $time = explode(" - ", $result->data->tanggal_masuk);
//                $printer->text("\n\n");
//                $printer->setTextSize(1, 1);
//                $printer->text("--- " . $result->data->kode . " ---\n");
//                $printer->text($tanggal[0] . "\n");
//                $printer->text(new format("Jam Pesan", $tanggal[1]));
//                $printer->text(new format("Jam Masuk", $tanggal_masuk[1]));
//                $printer->text(new format("NOPOL", $result->data->nomor_polisi));
//                $printer->text(new format("Kendaraan", $result->data->merek_kendaraan . " | " . $result->data->nama_kendaraan));
//                $printer->text("--------------------------------\n");
//
//                $printer->setJustification(Printer::JUSTIFY_LEFT);
//                foreach ($result->data->detail_jasa as $value) {
//                    $printer->text(new format("1 x ", $value->nama_jasa));
//                }
//
//                $printer->feed(2);
//                $printer->cut();
//                $printer->close();
//                return response()->json($result, 200);
//            } catch (Exception $e) {
//                CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Gagal, Printer bermasalah !!!", "danger");
//                return response()->json(['error' => true, 'msg' => 'Gagal Print', 'data' => null], 200);
//            }
        }
    }

    public function save()
    {
        $services = json_decode(stripslashes($_POST['services']));
        $response = Curl::to($this->baseurl . '/reservasi/ots')
            ->withData(array('id_cabang' => 1, 'id_kendaraan' => (int)$_POST['id_kendaraan'], 'services' => $services))
            ->asJson()
            ->post();

        if ($response->error) {
            return response()->json($response, 500);
        } else {
            $response = Curl::to($this->baseurl . '/reservasi/detail/' . $response->data)
                ->get();
            $result = json_decode($response);
            /*if ($result) {
                $logo = EscposImage::load('logo_black.png', false);

                try {
                    $printerName = Curl::to($this->baseurl . '/general/printerFrontName')->get();
                    $ip = Request::ip();
                    $profile = CapabilityProfile::load("simple");
                    $connector = new WindowsPrintConnector("smb://Guest@" . $ip . '/' . $printerName);
                    $printer = new Printer($connector);
                    $printer->setJustification(Printer::JUSTIFY_CENTER);
                    $printer->setTextSize(2, 2);
                    $printer->text("RODEO CAR WASH");
                    $printer->text("\n");
                    $printer->setTextSize(2, 2);
                    $printer->text($result->data->status);
                    $printer->text("\n\n");

                    $printer->qrCode($result->data->kode, Printer::QR_ECLEVEL_L, 11);
                    $tanggal = $time = explode(" - ", $result->data->tanggal);
                    $tanggal_masuk = $time = explode(" - ", $result->data->tanggal_masuk);
                    $printer->text("\n\n");
                    $printer->setTextSize(1, 1);
                    $printer->text("--- " . $result->data->kode . " ---\n");
                    $printer->text($tanggal[0] . "\n");
                    $printer->text(new format("Jam Pesan", $tanggal[1]));
                    $printer->text(new format("Jam Masuk", $tanggal_masuk[1]));
                    $printer->text(new format("NOPOL", $result->data->nomor_polisi));
                    $printer->text(new format("Kendaraan", $result->data->merek_kendaraan . " | " . $result->data->nama_kendaraan));
                    $printer->text("--------------------------------\n");

                    $printer->setJustification(Printer::JUSTIFY_LEFT);
                    foreach ($result->data->detail_jasa as $value) {
                        $printer->text(new format("1 x ", $value->nama_jasa));
                    }

                    $printer->feed(2);
                    $printer->cut();
                    $printer->close();
                    return response()->json($result, 200);
                } catch (Exception $e) {
                    CRUDBooster::redirect($_SERVER['HTTP_REFERER'], "Gagal, Printer bermasalah !!!", "danger");
                    return response()->json(['error' => true, 'msg' => 'Gagal Print', 'data' => null], 200);
                }
            }*/
            return response()->json($result, 200);
        }
    }

    public function done()
    {
        return view('terimakasih', [
            'title'   => 'Thx',
            'scripts' => ''
        ]);
    }

}

class format
{
    private $name;
    private $text;

    public function __construct($name = '', $text = '')
    {
        $this->name = $name;
        $this->text = $text;
    }

    public function __toString()
    {
        $rightCols = 20;
        $leftCols = 12;

        $left = str_pad($this->name, $leftCols);
        $right = str_pad($this->text, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}