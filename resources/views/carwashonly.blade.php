<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('Partials.head')
</head>
<body class="transaksi">

<div class="container-fluid">
    <div class="row position-relative mb-0" style="background-color: #00b2f3">
        <div class="p-2 position-absolute abs-custom">
            {{--<a href="{{ url('type').'/'.$merk }}" class="float-left pl-3 pr-3"><h4><i--}}
                            {{--class="fa fa-arrow-left text-white"></i>--}}
                {{--</h4></a>--}}
            <input type="hidden" value="{{$jamTutup}}" id="hours">
        </div>
        <div class="col-12 pt-2 pb-2">
            <h1 class="text-center text-white mb-0">Silahkan Pilih Layanan yang Anda Inginkan</h1>
        </div>
    </div>
    <div class="row" style="height:100%;">
        <div class="col-12 pt-0 pl-3 pr-3 pb-3 border border-grey col-custom-1 position-relative">
            <div class="runningtext custom bg-black-trans mt-4 mr-4 ml-4" style="height:90px;display: flex;align-items: center;"><h5 style="height: auto;">kami juga sediakan jasa carcare seperti engine cleaning, evaporator cleaning, ozonisasi dan lainnya</h5></div>
        <!--Carousel Wrapper-->
            <div id="multi-item-example" class="carousel slide carousel-multi-item mt-2 animated slow fadeIn"
                 data-ride="carousel" style="margin-bottom: 5px;">

                <!--Controls-->
                <div class="controls-top custom">
                    <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i
                                class="fas fa-chevron-left"></i></a>
                    <a class="btn-floating" href="#multi-item-example" data-slide="next"><i
                                class="fas fa-chevron-right"></i></a>
                </div>
                <!--/.Controls-->

                <!--Slides-->
                <div class="carousel-inner" role="listbox" style="margin-top: 0px">
                <?php $no = 1;?>
                @foreach ($carwash as $cw)
                    <!--slide-->
                        <?php if ($no == 1) {
                            $class = 'active';
                        } else {
                            $class = '';
                        }?>
                        <div class="carousel-item pl-4 pr-4 <?= $class?>">
                            <!--content-->
                            @foreach ($cw as $v)
                                <div class="col-item col-md-4 carwash" style="padding-top:12px;" id="carwash-{{$v->id}}"
                                     onclick="buy(this, '{{$v->id}}', '{{$v->nama}}','{{$v->harga}}')" style="">
                                    <a href="#" class="remove-mini" style="height: 360px;"></a>
                                    <a href="#" class=" position-relative">
                                        <div class="card" style="height: 360px;">
                                            <div class="card-body" style="display:flex; align-items:center;">
                                                    <h1 class="text-center font-weight-bolder" style="margin-bottom:6px; width:100%; color:#b32c2a;">{{"Rp " . number_format($v->harga, 0, ',', '.')}}</h1>
                                        </div>
                                        <div class="card-footer">
                                                    <h1 class="text-center font-weight-bolder" style="margin-bottom:6px; width:100%;">{{ucwords(strtolower($v->nama))}}</h1>
                                            
                                        </div>
                                        </div>
                                    </a>
                                </div>
                                <!--/.content-->
                            @endforeach

                        </div>
                        <!--/.slide-->
                        <?php $no++;?>
                    @endforeach
                </div>
                <!--/.Slides-->

            </div>
            <!--/.Carousel Wrapper-->
            <div class="col-12 pl-4 pr-4 pb-3 pt-3 text-center" style="position: fixed; bottom:0; left:0;">
                    <button class="btn btn-primary btn-custom-2" style="background-color: #00b2f3; width:96% !important;" id="confirm">PESAN
                    </button>
                </div>
        </div>
    </div>
</div>

<div id="domMessage" style="display:none;">
    <h5 style="float: none; margin: 0 auto;"><img src="{{asset('assets/images/loading.gif')}}"/> Transaksi sedang
        diproses ...</h5>
</div>

@include('Partials.footer')
<script type="text/javascript">
    var url_confirm = "<?= url('/save'); ?>";
    var url_done = "<?= url('/v2'); ?>";
    var services = [];
    var total = 0;
    var carwash = null;

    $(function () {
        $('.carousel').carousel({
            interval: false
        });
    });

    $(".col-item").on("click", function (e) {
        e.preventDefault();
    });

    function rupiah(value) {
        var sisa = value.toString().length % 3,
            rupiah = value.toString().substr(0, sisa),
            ribuan = value.toString().substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }

    function timeCheck() {
        var d = new Date();
        var h = ((d.getHours() < 10) ? '0' + d.getHours() : d.getHours());
        var m = ((d.getMinutes() < 10) ? '0' + d.getMinutes() : d.getMinutes());
        var now = h + ":" + m + ":00";

        return Date.parse('01/01/2019 ' + now) < Date.parse('01/01/2019 ' + $('#hours').val() + ":00");
    }

    function buy(element, id, service, price) {

        if ($(element).hasClass("selected")) {
            if ($(element).hasClass("carwash") && carwash != null) {
                carwash = null;
            }
            $(element).removeClass('selected');
            $("#" + id).remove();
            total -= parseInt(price);
            $("#total").text("Rp " + rupiah(total));
            services.splice($.inArray(parseInt(id), services), 1);
            console.log(services);
        } else {
            if ($(element).hasClass("carwash")) {
                if (carwash != null) {
                    $("#carwash-" + carwash).click();
                }

                carwash = id;
                $(element).addClass('selected');
                $('#cart').append(
                    "<tr id='" + id + "'>" +
                    "   <td> <span class=\"quantity\">1</span> </td>" +
                    "   <td>" + service + "</td>" +
                    "   <td><span class=\"price\">Rp " + rupiah(price) + "</span></td>" +
                    "</tr>");

                total += parseInt(price);
                $("#total").text("Rp " + rupiah(total));
                services.push(parseInt(id));
                console.log(services);
            } else if ($(element).hasClass("carcare")) {
                if (timeCheck()) {
                    carwash = 'selected';
                    $(element).addClass('selected');
                    $('#cart').append(
                        "<tr id='" + id + "'>" +
                        "   <td> <span class=\"quantity\">1</span> </td>" +
                        "   <td>" + service + "</td>" +
                        "   <td><span class=\"price\">Rp " + rupiah(price) + "</span></td>" +
                        "</tr>");

                    total += parseInt(price);
                    $("#total").text("Rp " + rupiah(total));
                    services.push(parseInt(id));
                }
            }
        }
    }

    $('#confirm').on('click', function () {
        if (services.length > 0) {
            $.blockUI({message: $('#domMessage')});
            $.ajax({
                method: "POST",
                url: url_confirm,
                data: {
                    id_kendaraan: 91,
                    services: JSON.stringify(services)
                },
                cache: false,
                success: function (resp) {
                    $.unblockUI();
                    window.location = url_done;
                },
                error: function (data) {
                    $.unblockUI();
                }
            });
        } else {
            $.confirm({
                title: 'Perhatian',
                content: 'Mohon pilih jasa terlebih dahulu',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        btnClass: 'col-md-12 btn blue-gradient',
                        action: function () {}
                    }
                }
            });
        }
    });

    $('.carousel').carousel({
        interval: false
    })
</script>
</body>
</html>
