<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('Partials.head')
</head>
<body>
<div class="container-fluid mb-5">
    <div class="row position-relative" style="background-color: #00b2f3">
        <div class="p-2 position-absolute abs-custom">
            <a href="{{ url('/') }}" class="float-left pl-3 pr-3"><h4><i class="fa fa-arrow-left text-white"></i>
                </h4></a>
        </div>
        <div class="col-12 pt-2 pb-2">
            <h2 class="text-center text-white mb-0">Silahkan Pilih Tipe Kendaraan Anda</h2>
        </div>
    </div>
    {{--<div class="row bg-black-trans pt-1 pb-1">
        <div class="col-md-12 position-relative">
            <div class="runningtext full">
                <h5>Promosikan usaha/produk anda di sini dengan menggunakan iklan running text. Promosikan usaha/produk
                    anda di sini dengan menggunakan iklan running text. </h5>
            </div>
        </div>
    </div>--}}
</div>
<div class="container">
    <div class="row" style="height:100%;">
        {{--@foreach ($types[0] as $t)
            <div class="col-md-3 slow fadeIn animated" style="height: 175px">
                <a href="{{ url('transaksi').'/'.$merk.'/'.$t->id }}" class=" position-relative">
                    <div class="card custom mb-2 text-center mb-5" style="height: 155px; background-color: #4cc9f6">
                        <div class="card-body">
                            <img src="{{$t->gambar}}" style="height: 80px; width: auto">
                        </div>
                        <div class="card-footer">
                            <h5 class="card-title">{{$t->keterangan}}</h5>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach--}}

        <!--Carousel Wrapper-->
        <div id="multi-item" class="carousel slide carousel-multi-item animated slow fadeIn mb-0"
             data-ride="carousel">

            <!--Controls-->
            <div class="controls-top custom">
                <a class="btn-floating" href="#multi-item" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                <a class="btn-floating" href="#multi-item" data-slide="next"><i
                            class="fas fa-chevron-right"></i></a>
            </div>
            <!--/.Controls-->

            <!--Slides-->
            <div class="carousel-inner" role="listbox">
            <?php $no = 1;?>
            @foreach ($types as $cc)
                <!--slide-->
                    <?php if ($no == 1) {
                        $class = 'active';
                    } else {
                        $class = '';
                    }?>
                    <div class="carousel-item pl-4 pr-4 <?= $class?>" style="min-height:525px !important;">
                        <!--content-->
                        @foreach ($cc as $v)
                            <div class="col-md-3 slow fadeIn animated" style="height: 175px; margin-top: 10px">
                                <a href="{{ url('transaksi').'/'.$merk.'/'.$v->id }}" class=" position-relative">
                                    <div class="card custom mb-2 text-center mb-5" style="height: 155px; ">
                                        <div class="card-body">
                                            <img src="{{$v->gambar}}" style="height: 105px; width: auto">
                                        </div>
                                        <div class="card-footer" style="height: 50px; background-color: #4cc9f6; border-bottom-left-radius: 18px; border-bottom-right-radius: 18px;">
                                            <h6 class="card-title col-centered" style="color: white; font-weight: bold; margin-bottom:0 !important;">{{$v->keterangan}}</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!--/.content-->
                        @endforeach

                    </div>
                    <!--/.slide-->
                    <?php $no++;?>
                @endforeach
            </div>
            <!--/.Slides-->

        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>
@include('Partials.footer')
<<script>
    $(function () {
        $('.carousel').carousel({
            interval: false
        });
    });
</script>
</body>
</html>
