<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>RODEO CARWASH</title>

{{--<link rel="stylesheet" type="text/css" href="{{asset('assets/css/app.css')}}">--}}
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/node_modules/mdbootstrap/css/mdb.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/node_modules/mdbootstrap/css/modules/animations-extended.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery-confirm.min.css?new')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/compiled-4.8.8.min.css?new1')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css?new4')}}">
