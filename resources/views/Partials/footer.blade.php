<script type="text/javascript" src="{{asset('assets/js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/node_modules/mdbootstrap/js/mdb.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/node_modules/mdbootstrap/js/modules/forms-free.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/agile-min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-confirm.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.blockUI.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('input.form-control').on('focus', function () {
            $(this).toggleClass('active');
        });
    });
</script>

