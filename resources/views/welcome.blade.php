<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('Partials.head')
</head>
<body>

<!-- Main navigation -->
<header>
    <!-- Full Page Intro -->
    <div class="container-fluid position-absolute zi-1">
        <div class="row bg-black-trans pt-1 pb-1">
            <div class="col-md-12 position-relative">
                <div class="runningtext full">
                    <h5 class="text-white">Promosikan usaha/produk anda di sini dengan menggunakan iklan running text.
                        Promosikan usaha/produk anda di sini dengan menggunakan iklan running text. </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="view jarallax custom fadeIn animated"
         style="background-image: url('{{asset('assets/images/bg-carwash.jpg')}}'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <!-- Mask & flexbox options-->
        <div class="mask rgba-gradient-green d-flex justify-content-center align-items-center">
            <!-- Content -->
            <div class="container">
                <!--Grid row-->
                <div class="row justify-content-center">
                    <div class="col-8">
                        <section class="form-elegant slow bounceInDown animated">
                            <div class="card bg-transparant" style="">
                                <div class="card-body mx-4">

                                    <div class="row delay-2s fadeIn animated">
                                        <div class="col-12">
                                            <div class="align-items-center">
                                                <div class="text-center">
                                                    <img src="{{asset('assets/images/car-wash.png')}}" class="m-lr-auto"
                                                         style="width: 150px; height: auto">
                                                </div>
                                                <h1 class="display-4 text-center text-white mt-2">Rodeo Car Wash</h1>
                                            </div>
                                        </div>
                                    </div>
                                    <form>
                                        <div class="md-form delay-2s fadeIn animated">
                                            <input type="text" id="kodebooking" class="form-control text-white">
                                            <label for="kodebooking" class=" text-white">KODE BOOKING</label>
                                        </div>

                                        <div class="text-center mb-3 delay-2s fadeIn animated">
                                            <a href="#" id="cetak" target="_blank"
                                               class="btn blue-gradient btn-block btn-rounded z-depth-1a waves-effect waves-light">Cetak</a>
                                            <span class="d-block mt-0 mb-3 text-white">Atau</span>
                                            <a href="{{ url('merk') }}"
                                               class="btn purple-gradient btn-block btn-rounded z-depth-1a waves-effect waves-light">Booking
                                                Baru</a>
                                        <!-- <a href="{{ url('print') }}" target="_blank" class="btn btn-success btn-custom-1 shadow">CETAK</a> -->
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </section>

                    </div>
                </div>
                <!--Grid row-->
            </div>
            <!-- Content -->
        </div>
        <!-- Mask & flexbox options-->
    </div>
    <!-- Full Page Intro -->
</header>
<!-- Main navigation -->
@include('Partials.footer')

<script>

    $("#cetak").on("click", function (e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/print',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                code: $('#kodebooking').val()
            },
            // beforeSend: function () {
            //     if (currentRequest != null) {
            //         currentRequest.abort();
            //     }
            // },
            success: function (resp) {
                console.log(resp);
                if (resp != null) {
                    alert("Berhasil Cetak [" + resp.data.kode + ", " + resp.data.total + "]");
                } else {
                    alert("Kode Booking Salah");
                }
            },
            error: function (data) {
                console.log(data);
                alert("Something Gone Wrong");
            }
        });
    });
</script>

</body>
</html>
