$(document).ready(function () {

    $("form").submit(function (event) {
        event.preventDefault();
        var $form = $(this),
            code = $form.find("input[name='code']").val();

        $.ajax({
            method: "POST",
            url: '/print',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                code: code
            },
            // beforeSend: function () {
            //     if (currentRequest != null) {
            //         currentRequest.abort();
            //     }
            // },
            success: function (resp) {
                console.log(resp);
                if (resp != null) {
                    alert("Berhasil Cetak [" + resp.data.KODE_BOOKING + ", " + resp.data.TOTAL + "]");
                    console.log(resp.data);
                } else {
                    alert("Kode Booking Salah");
                }
            },
            error: function (data) {
                console.log(data);
                alert("Something Gone Wrong");
            }
        });
    });
});